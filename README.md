Affiliator
==========

Associate drupal users with 3rd party affiliates.
Save payment data to affiliates and configure special events that affiliate users may trigger.

Dependencies
------------

Requires [php-eventemitter](https://github.com/hfcorriez/php-eventemitter) installed using [composer](https://getcomposer.org/).

Installation
------------

Copy the affiliator folder to your Drupal */sites/all/modules* directory.
Enable the Affiliator module.

Usage
-----

By default, visiting the site with an `?affiliateId=...` will associate an authenticated user with that affiliate id.
If the user is anonymous, then it will store that information in their session.

Upon registration, an affiliated user will emit a `user.registered` event.
This event can be used to track conversion, make payouts to affiliates, etc.

Functions
---------

**affiliator_create_event($name, $options)**

Creates an event. By default, the event is associated with the current user (or session), and timestamp.

**affiliator_create_user($options)**

Creates an affiliate user. By default, associates the current user with the specified affiliate id/referer.

**affiliator_data()**

Returns affiliate data for the current user.

***theme*_affiliator_get_affiliate_id()**

Override this hook to change how the affiliate id is read. By default, the module checks the page's `?affiliateId` query parameter.

***theme*_affiliator_get_affiliate_referer()**

Override this hook to change how the affiliate referer is read. By default, the module checks the http referer.

**affiliator_make_payment($options)**

Save a payment to an affiliate in the database. You should specify an amount, currency, affiliateId and optionally a status and some event Ids that may be associated with the payment.

**affiliator_update_payment($paymentId, $options)**

Alter the status of a current payment.

**affiliator_on_event($name, $callback)**

Handle an affiliate event with the specified name.

**affiliator_user_is_affiliated()**

Return `true` if the current user has associated with an affiliate. `false` otherwise.
